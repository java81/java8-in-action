package com.eara.tutorial.functionalinterfaces.consumer;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

/**
 * The Consumer<T> interface defines an abstract method
 * named "accept" that takes an object of generic type T
 * and returns no result (void).
 * It's useful when we need to access an object of type T
 * and perform some operations on it.
 *
 * In this example, we use a forEach method combined with a
 * lambda to print all the elements of a list.
 */
public class WorkingWithConsumer {

    public static void main(String[] args) {
        // The lambda is the implementation of the accept method from Consumer
        forEach(Arrays.asList(1, 2, 3, 4, 5),
                (Integer i) -> System.out.println(i * 2));
    }

    public static <T> void forEach(List<T> list, Consumer<T> c) {
        for (T t : list) {
            c.accept(t);
        }
    }
}
