package com.eara.tutorial.functionalinterfaces.predicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * The Predicate<T> interface defines an abstract method
 * named "test" that accepts an object of generic type T
 * and returns a boolean.
 */
public class WorkingWithPredicates {

    public static void main(String[] args) {

        List<String> listOfStrings = Arrays.asList("", "Coco", "", "Mami", "", "", "Paola");

        Predicate<String> nonEmptyStringPredicate = (String s)-> !s.isEmpty();

        List<String> nonEmpty = filter(listOfStrings, nonEmptyStringPredicate);

        for (String s : nonEmpty) {
            System.out.println(s);
        }
    }

    public static <T> List<T> filter(List<T> list, Predicate<T> p) {
        List<T> results = new ArrayList<T>();

        for(T s : list) {
            if(p.test(s)) {
                results.add(s);
            }
        }

        return results;
    }
}
