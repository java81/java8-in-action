package com.eara.tutorial.lambdas;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * This functional interface matches the signature
 * BufferedReader -> String
 * and may throw an IOException
 */
// 2) Use a functional interface to pass behaviors
@FunctionalInterface
public interface BufferedReaderProcessor {
    String process(BufferedReader br) throws IOException;
}
