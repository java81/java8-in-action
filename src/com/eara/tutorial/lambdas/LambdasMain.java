package com.eara.tutorial.lambdas;

import com.eara.tutorial.behavior.parameterization.domain.Apple;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Comparator;
/**
 * A lambda expression is composed of:
 * - List of parameters.
 * - An arrow.
 * - Body
 *
 * The basic syntax of a lambda is either:
 * (parameters) -> expression
 * or
 * (parameters) -> {statements;}
 *
 * You can use a lambda expression in the context of a functional interface.
 * A functional interface is an interface that specifies exactly one abstrac method.
 *
 * Lambda expressions let you provide the implementation of the abstract method of a
 * functional interface directly inline and treat the whole expression as an instance
 * of a functional interface.
 *
 * Steps to apply the execute around pattern:
 * 1) Remember behavior parameterization
 * 2) Use a functional interface to pass behaviors
 * 3) Execute a behavior
 * 4) Pass lambdas
 */
public class LambdasMain {
    public static void main(String[] args) throws Exception {

        Comparator<Apple> byWeight = (Apple apple1, Apple apple2) -> {
          return apple1.getWeight() - apple2.getWeight();
        };

        process(() -> System.out.println("This is awesome!"));

        // 4) Pass lambdas
        String oneLine = processFile((BufferedReader br)->
                br.readLine());

        System.out.println(oneLine);

        String twoLines = processFile((BufferedReader br)->
                br.readLine() + "\n" + br.readLine());

        System.out.println(twoLines);
    }

    public static void process(Runnable r) {
        r.run();
    }

    // 1) Remember behavior parameterization
    public static String processFile(BufferedReader br) throws Exception {
        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader("/home/eara/Documenti/Libri/README.md"))) {
            // this is the line that does usefull work.
            return br.readLine();
        }
    }

    // 3) Execute a behavior
    public static String processFile(BufferedReaderProcessor p) throws Exception {
        try (BufferedReader bufferedReader = new BufferedReader(
                new FileReader("/home/eara/Documenti/Libri/README.md"))) {
            // this is the line that does usefull work.
            return p.process(bufferedReader);
        }
    }
}
