package com.eara.tutorial.behavior.parameterization.strategies;

import com.eara.tutorial.behavior.parameterization.domain.Apple;

/**
 * This interface represents the algorithms family
 * for Apple objects.
 */
public interface ApplePredicate {
    boolean test(Apple apple);
}
