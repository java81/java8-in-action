package com.eara.tutorial.behavior.parameterization.strategies;

import com.eara.tutorial.behavior.parameterization.domain.Apple;

public class AppleGreenColorPredicate  implements ApplePredicate {

    @Override
    public boolean test(Apple apple) {
        return "green".equals(apple.getColor());
    }
}
