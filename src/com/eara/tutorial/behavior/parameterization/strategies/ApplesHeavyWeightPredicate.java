package com.eara.tutorial.behavior.parameterization.strategies;

import com.eara.tutorial.behavior.parameterization.domain.Apple;

public class ApplesHeavyWeightPredicate  implements ApplePredicate {
    @Override
    public boolean test(Apple apple) {
        return apple.getWeight() > 150;
    }
}
