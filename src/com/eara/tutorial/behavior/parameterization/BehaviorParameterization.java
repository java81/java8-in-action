package com.eara.tutorial.behavior.parameterization;

import com.eara.tutorial.behavior.parameterization.domain.Apple;
import com.eara.tutorial.behavior.parameterization.strategies.AppleGreenColorPredicate;
import com.eara.tutorial.behavior.parameterization.strategies.ApplePredicate;
import com.eara.tutorial.behavior.parameterization.strategies.ApplesHeavyWeightPredicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BehaviorParameterization {

    public static void main(String[] args) {
        List<Apple> inventory = Arrays.asList(
                new Apple("green", 180), new Apple("yellow", 20),
                new Apple("red", 70));

        List<Apple> result = filterApples(inventory, new AppleGreenColorPredicate());

        for (Apple apple : result) {
            System.out.println(apple.getColor());
        }

        List<Apple> result1 = filterApples(inventory, new ApplesHeavyWeightPredicate());

        for (Apple apple : result1) {
            System.out.println(apple.getWeight());
        }
    }

    /**
     * Filtering by abstract criteria.
     */
    public static List<Apple> filterApples(List<Apple> inventory, ApplePredicate p) {
        List<Apple> result = new ArrayList<>();

        for (Apple apple: inventory) {
            if(p.test(apple)) {
                result.add(apple);
            }
        }

        return result;
    }
}

